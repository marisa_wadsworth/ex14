﻿using System;

namespace ex14
{
    class Program
    {
        static void Main(string[] args)
        {

            //Start the program with Clear();
            Console.Clear();
            
            // Decleare Variables
            // guestName : string
            // birthMonth : string

            string guestName;
            string guestbirthMonth;

            //Introducing myself with my name.
            //Print out a question "What is your name?"
            Console.WriteLine("Hello my name is Marisa");
            Console.WriteLine("What is your name ?");

            //read the person's name.
            //Place the string inside the guestName Variables
            guestName = Console.ReadLine();
            Console.WriteLine();

            //Welcome the person to the program
            // Ask them what is there birth month
            Console.WriteLine($"Hello {guestName} What is your birth month?");
            
            //Read out the answer
            guestbirthMonth = Console.ReadLine();
            Console.WriteLine();

            //If answer.
            //If the guest has the same birthday as me.
            if(guestbirthMonth == "August" || guestbirthMonth == "august" || guestbirthMonth == "aug")
            {
                //Print out a messgae
                Console.WriteLine("YAY ! we have the same birth month");
                Console.WriteLine();
            }

            //else
            //If guest has a different answer.
            //print out my birth month
            else
                {
                    Console.WriteLine("That's Cool ! My birth month is in August");
                }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
